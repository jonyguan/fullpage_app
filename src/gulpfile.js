'use strict';
var gulp       = require('gulp'),
    less       = require('gulp-less'),
    mincss     = require('gulp-minify-css'),
    autoprefix = require('gulp-autoprefixer'),
    plumber    = require('gulp-plumber');


/*编译less文件*/
gulp.task('less',function(){
    gulp.src(['less/**/*.less','!less/common/module.less'])
        .pipe(plumber())
        .pipe(less())
        .pipe(autoprefix('last 2 versions'))
        .pipe(gulp.dest('css/'));
});

/*监听less文件*/
gulp.task('watch',function(){
    return gulp.watch('less/**/*.less',['less']);
});